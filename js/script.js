$(document).ready(function () {
    $('.portfolio__slider').slick({
        arrows: false,
        dots: true,
        adaptiveHeight: true,
        slidesToShow: 3,
        infinite: true,
        slidesToScroll: 1,
        speed: 1000,
        easing: 'ease',
        autoplay: true,
        autoplaySpeed: 2000,
        centerMode: false,
        // variableWidth: true
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    variableWidth: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerMode: true,
                }
            },
        ]
    });
})

$(document).ready(function () {
    $('.feedbackSlider').slick({
        arrows: false,
        dots: false,
        adaptiveHeight: true,
        slidesToShow: 1,
        infinite: true,
        slidesToScroll: 1,
        variableWidth: true,
        responsive: [
            {
                breakpoint: 850,
                settings: {
                    variableWidth: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
        ]
    });
})

function changeBtnText(btnId) {
    let btn = document.getElementById(btnId);
    if (btn.innerText == "Подробнее") btn.innerText = "Скрыть";
    else btn.innerText = "Подробнее";
}

$(document).ready(function () {

    $('.first-button').on('click', function () {

        $('.animated-icon1').toggleClass('open');
    });
    $('.second-button').on('click', function () {

        $('.animated-icon2').toggleClass('open');
    });
    $('.third-button').on('click', function () {

        $('.animated-icon3').toggleClass('open');
    });
});
